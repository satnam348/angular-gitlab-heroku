import { Component, OnInit, Input } from '@angular/core'
import { UseCase } from '../../usecase.model'

@Component({
  selector: 'app-usecase',
  templateUrl: './usecase.component.html'
})
export class UsecaseComponent implements OnInit {
  @Input() useCase: UseCase

  constructor() {}

  ngOnInit() {}
}
